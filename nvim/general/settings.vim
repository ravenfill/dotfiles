syntax enable                           

call plug#begin('~/.config/nvim/plugged')

Plug 'ghifarit53/sonokai'
Plug 'mcchrish/nnn.vim'
Plug 'ap/vim-css-color'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'itchyny/lightline.vim'

call plug#end()

set termguicolors

let g:sonokai_style = 'espresso'
let g:sonokai_enable_italic = 1
let g:sonokai_disable_italic_comment = 1
let g:lightline = {'colorscheme' : 'sonokai'}

colorscheme sonokai

set tabstop=4 softtabstop=4
set nowrap
set number
set noshowmode
set tabstop=4
set shiftwidth=4
set smarttab                            
set expandtab
set noshowmode
